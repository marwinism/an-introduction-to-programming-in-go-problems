package main

import "fmt"

func main() {
	for i := 1; i <= 10; i++ {
		if i%2 == 0 {
			fmt.Println(i, "is even")
		} else {
			fmt.Println(i, "is odd")
		}
	}

	numberConverter(3)
	testif()
	big3()
	fizzbuzz()
}

func numberConverter(i int) {
	switch i {
	case 0:
		fmt.Println("Zero")
	case 1:
		fmt.Println("One")
	case 2:
		fmt.Println("Two")
	default:
		fmt.Println("Unknown")
	}
}

func testif() {
	i := 10
	if i > 10 {
		fmt.Println("Big")
	} else {
		fmt.Println("Small")
	}
}

func big3() {
	for i := 1; i <= 100; i++ {
		if i%3 == 0 {
			fmt.Println(i)
		}
	}
}
func fizzbuzz() {
	for i := 1; i <= 100; i++ {
		if i%3 == 0 {
			fmt.Println(i, "Fizz")
		}
		if i%5 == 0 {
			fmt.Println(i, "Buzz")
		}
		if i%3 == 0 && i%5 == 0 {
			fmt.Println(i, "FizzBuzz")
		}
	}
}

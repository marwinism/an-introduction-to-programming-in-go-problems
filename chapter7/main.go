package main

import "fmt"

var x int = 5

func main() {
	xs := []float64{98, 93, 77, 82, 83}
	fmt.Println(averageArrayFloat64(xs))
	f()
	fmt.Println(f1())
	//assign the returns directly
	x, y := f3()
	fmt.Println(x, y)
	//args
	fmt.Println(add(1, 2, 3))
	fmt.Println(add(1, 2))
	fmt.Println(add(1))
	fmt.Println(add(1, 3, 4, 65, 76, 7))

	//println function example: func Println(a ...interface{}) (n int, err error)
	xy := []int{1, 2, 3, 4, 5}
	fmt.Println(add(xy...))

	//function inside function
	z := 0
	increment := func() int {
		z++
		return z
	}
	//Incrementing from internal function in main
	fmt.Println(increment())
	fmt.Println("Scary internal function passing")
	nextEven := makeEvenGenerator()
	fmt.Println(nextEven())
	fmt.Println(nextEven())
	fmt.Println(nextEven())

	//defering
	fmt.Println("Defering")
	defer second() //after main function is finished, defer to second() function
	first()
	//recursion
	fmt.Println("recursion")
	fmt.Println(factorial(2))
	omg()

	fmt.Println(ishalf(1))
	fmt.Println(ishalf(2))
	fmt.Println("Greatest number is", findGreatestNumber(1, 2, 3, 4, 5, 99, 7, 8, 9))

	nextOdd := makeOddGenerator()
	fmt.Println(nextOdd())
	fmt.Println(nextOdd())
	fmt.Println(nextOdd())
	fmt.Println(nextOdd())
	fmt.Println(nextOdd())
	fmt.Println(fib(2))
}

func averageArrayFloat64(xs []float64) float64 {
	//panic("Not implemented")
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}

//global variable
func f() {
	fmt.Println(x)
}

// Stack Example
func f1() int {
	return f2()
}

// f1 -> function name, (primary)->input type, (secondary)->output type
func f2() (r int) {
	r = 1
	return r
}

//return two ints from function
func f3() (int, int) {
	return 5, 6
}

//arg im a pirate!
func add(args ...int) int {
	total := 0
	for _, v := range args {
		total += v
	}
	return total
}

//this is some scary shit man, a function creating a function and returning that function
// func Name (input) (return output functionINTERNAL(input) (return outputINTERNAL))
func makeEvenGenerator() func() uint {
	i := uint(0)               // init uint to 0 and input it into the internal function
	return func() (ret uint) { //function gets return "ret"
		ret = i //assing i to ret which is 0
		i += 2  // increment "i" with +2
		return  //call return so the ret is returned, ret is 0 first time
	} //remember, "i" is still in memory already incrementet with 2
	//second time function will call, ret will be assigned to i, which is 2
	//and it will increment +2, so next time ret will be 4
	//rather smart accualy
}

func first() {
	fmt.Println("1st")
}
func second() {
	fmt.Println("2nd")
}

func factorial(x uint) uint {
	if x == 0 {
		return 1
	}

	return x * factorial(x-1)
}

//panic recovery
func omg() {
	defer func() {
		str := recover()
		fmt.Println(str)
	}()
	panic("Panic")
}

/* wat?
func sum (a ...interface{}) (n int, err error){

}*/

// Write a function which takes an integer and halves it and returns true if it was even or false if it was odd.
func ishalf(n int) (retn int, ret bool) {
	number := n / 2
	number = number % 2
	var b bool
	if number == 0 {
		b = false
	} else {
		b = true
	}

	return number, b
}

//Write a function with one variadic parameter that finds the greatest number in a list of numbers.
func findGreatestNumber(args ...int) (ret int) {
	n := 0
	for _, v := range args {
		if v > n {
			n = v
		}
	}
	return n
}

//Using makeEvenGenerator as an example, write a makeOddGenerator
//i dont think this is a good odd generator...
func makeOddGenerator() func() uint {
	i := uint(1)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

//this killed my computer so i guess it works
func fib(x uint) uint {
	return fib(x-1) + fib(x-2)
}

//What are defer, panic and recover? How do you recover from a run-time panic?
//pair recovery with defer

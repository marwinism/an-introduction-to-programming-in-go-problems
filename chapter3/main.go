package main

import "fmt"

func main () {
	fmt.Println("1 + 1=", 1.0+1.0)
	chapter2()
	boolstuff()
	computenumber()
	findlength()
	fmt.Println((true && false) || (false && true) || !(false && false))
}

func chapter2 (){
	fmt.Println("Hello world")
	//returns byte of e index.1
	fmt.Println("Hello World"[1])
	fmt.Println("Hello " + "World")
}

func boolstuff(){
	fmt.Println(true && true)
	fmt.Println(true && false)
	fmt.Println(true || true)
	fmt.Println(true || false)
	fmt.Println(!true)
}

func computenumber(){
	var i int64 = 321325*424521
	fmt.Println(i)
}

func findlength(){
	var mystring = "Hello you faggot"
	fmt.Println(len(mystring))
}

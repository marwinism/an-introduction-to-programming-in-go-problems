package main

import "fmt"

func main() {
	var x [5]int
	x[4] = 100
	fmt.Println(x)
	testscore()
	slices()
	sliceappend()
	copyslice()
	maps()
	atomstuff()
	atomstuffShorter()
	atomstuffMapinMap()
	fourththElement()
	givenArray()
	findSmallestNumber()
}

func testscore() {
	var x [4]float64
	x[0] = 94
	x[1] = 11
	x[2] = 94
	x[3] = 22

	y := [4]float64{
		98,
		32,
		43,
		65,
		//43,
	}

	var total float64 = 0
	for i := 0; i < len(x); i++ {
		total += x[i]
	}
	fmt.Println(total / float64(len(x)))

	total = 0
	for _, value := range y {
		total += value
	}
	fmt.Println(total / float64(len(x)))

}

func slices() {
	x := make([]float64, 5, 10)
	fmt.Println(x)

	arr := [5]float64{1, 2, 3, 4, 5}
	y := arr[0:5]
	fmt.Println(y)
}

func sliceappend() {
	slice1 := []int{1, 2, 3}
	slice2 := append(slice1, 4, 5)
	fmt.Println(slice1, slice2)
}

func copyslice() {
	slice1 := []int{1, 2, 3}
	slice2 := make([]int, 2)
	copy(slice2, slice1)
	fmt.Println(slice1, slice2)
}

func maps() {
	x := make(map[int]int)
	x[1] = 10
	fmt.Println(x)
	delete(x, 1)
	fmt.Println(x)
}

func atomstuff() {
	elements := make(map[string]string)
	elements["H"] = "Hydrogen"
	elements["He"] = "Helium"
	elements["Li"] = "Lithium"
	elements["Co"] = "Cock" //as in male chicken

	fmt.Println(elements)
	fmt.Println(elements["Co"])

	name, ok := elements["Un"]

	fmt.Println(name, ok)

	if name, ok := elements["Un"]; ok {
		fmt.Println(name, ok)
	}

	if name, ok := elements["Co"]; ok {
		fmt.Println(name, ok)
	}
}

func atomstuffShorter() {
	elements := map[string]string{
		"H":  "Hygrogen",
		"He": "Helium",
		"Co": "Cock",
	}

	fmt.Println(elements)
}

func atomstuffMapinMap() {
	elements := map[string]map[string]string{
		"H": map[string]string{
			"name":  "Hydrogen",
			"state": "gass",
		},

		"Co": map[string]string{
			"name":  "Cock",
			"state": "living",
		},
	}

	fmt.Println(elements)
	if el, ok := elements["Li"]; ok {
		fmt.Println(el["name"], el["state"])
	}

	if el, ok := elements["Co"]; ok {
		fmt.Println(el["name"], el["state"])
	}
}

func fourththElement() {
	arr := [4]int{1, 2, 3, 4}
	fmt.Println(arr[3])
}

func slicelength() {
	slice := make([]int, 3, 9) //makes slice from index 3 to 9
	fmt.Println(len(slice))
}

func givenArray() {
	x := [6]string{"a", "b", "c", "d", "e", "f"}
	fmt.Println(x[2:5])
}

func findSmallestNumber() {
	x := []int{
		48, 96, 86, 68,
		57, 82, 63, 70,
		37, 34, 83, 27,
		19, 97, 9, 17,
	}
	var number int = x[0]

	for i := 0; i < len(x); i++ {
		if x[i] < number{
			number = x[i]
		}
	}
	fmt.Println("lowest number in array: ", number)
}
